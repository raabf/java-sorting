/**
 * 
 */
package quicksort;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

/**
 * @author raabf
 * 
 */
public class Quickselect {

	public static <E> E quickselect(List<E> list, Comparator<E> comp,
			PivotSelector selector, int kthElem) {

		return quickselect(list, comp, selector, kthElem, kthElem);
	}

	public static <E> E quickselect(List<E> list, Comparator<E> comp,
			PivotSelector selector, int fromKthElem, int toKthElem) {
		return null;
	}

	@SuppressWarnings("unchecked")
	private static <E> int partition(List<E> list, int pivotIndex,
			Comparator<E> comp, PivotSelector selector, int fromKthElem,
			int toKthElem) {

		int range = toKthElem - fromKthElem;

		// swap pivot elements to the end of the List
		for (int i = pivotIndex; i < pivotIndex + range; i++) {

			Collections.swap(list, i, list.size() - range + i);
		}

		E fromElem = list.get(0);
		E toElem = list.get(list.size() - 1 - range);

		int i = 0;
		int j = list.size() - 1 - range;
		do {
			do
				i++;
			while (0 < comp.compare(list.get(i), fromElem));

			do
				j--;
			while (0 < comp.compare(toElem, list.get(j)));
		} while (i < j);

		return 0;

	}
}
